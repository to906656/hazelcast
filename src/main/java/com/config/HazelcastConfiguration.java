package com.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.config.NearCacheConfig;
import com.hazelcast.core.HazelcastInstance;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class HazelcastConfiguration {

    @Bean
    public ClientConfig hazelCastConfig() {
        /*Config config = new Config();*/
       /* config.getNetworkConfig().getJoin().getTcpIpConfig().addMember("192.168.225.29 ").setEnabled(true);
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);*/
       /* config.getNetworkConfig().setPublicAddress().setPort( 5900 )
                .setPortAutoIncrement( false );*/
       /* config.setInstanceName("hazelcast-instance")
                .addMapConfig(
                        new MapConfig()
                                .setName("configuration")
                                .setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
                                .setEvictionPolicy(EvictionPolicy.LRU)
                                .setTimeToLiveSeconds(-1));
        return config;*/
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getNetworkConfig().addAddress("172.30.124.159").setConnectionTimeout(50000);
        NearCacheConfig ncc = clientConfig.getNearCacheConfig("my-map");
        if(ncc == null){
            ncc = new NearCacheConfig();
        }
        //ncc.setCacheLocalEntries(true);
        ncc.setEvictionPolicy("LRU");
        ncc.setMaxSize(500000);
        ncc.setInvalidateOnChange(true);
        Map<String, NearCacheConfig> nearCache =  new HashMap<String, NearCacheConfig>();
        nearCache.put("CitiesLocal", ncc);
        clientConfig.addNearCacheConfig("my-map", ncc);
       // HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);
        return clientConfig;
    }
}
